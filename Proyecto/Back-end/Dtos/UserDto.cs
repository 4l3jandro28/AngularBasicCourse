namespace WebApi.Dtos
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Avatar {get; set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public bool IsAdministrator { get; set; }
        public bool IsCool { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}