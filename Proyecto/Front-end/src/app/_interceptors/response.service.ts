import { Injectable, Injector } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import { AuthenticationService } from '../_services/authentication.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class ResponseService implements HttpInterceptor {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  intercept(request: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {
    return httpHandler.handle(request).do((event: HttpEvent<any>) => {
      if(event instanceof HttpResponse) { }
    }, (error: any) => {
      if(error instanceof HttpErrorResponse){
        this.snackBar.open(error.message, 'OK');
      }
    });
  }
}
