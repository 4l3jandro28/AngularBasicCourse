import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthenticationService } from '../_services/authentication.service';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class RequestService implements HttpInterceptor {

  private authenticationService: AuthenticationService;

  constructor(
    private injector: Injector) { }

  intercept(request: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {
    this.authenticationService = this.injector.get(AuthenticationService);

    if(this.authenticationService.isAuthenticated()) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + this.authenticationService.getToken()
        }
      })
    }

    return httpHandler.handle(request);
  }
}
