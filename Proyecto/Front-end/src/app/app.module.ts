import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, JsonpModule, Jsonp } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { AppConfig } from './app.config';

//Guards
import { AuthenticatedGuard } from './_guards/authenticated.guard';
import { NoAuthenticatedGuard } from './_guards/no-authenticated.guard';

//Services
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';

//Interceptors
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestService } from './_interceptors/request.service';
import { ResponseService } from './_interceptors/response.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CdkTableModule } from '@angular/cdk/table';
import { FlexLayoutModule } from '@angular/flex-layout';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';

//Components
import { DialogComponent } from './_components/dialog/dialog.component';
import { ToolbarComponent } from './_shared-components/toolbar/toolbar.component';
import { SidenavComponent } from './_shared-components/sidenav/sidenav.component';
import { LoginComponent } from './_pages/login/login.component';
import { RegisterComponent } from './_pages/register/register.component';
import { HomeComponent } from './_pages/home/home.component';
import { IndexComponent } from './_pages/users/index/index.component';
import { DetailComponent } from './_pages/users/detail/detail.component';
import { GridComponent } from './_pages/grid/grid.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogComponent,
    ToolbarComponent,
    SidenavComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    IndexComponent,
    DetailComponent,
    GridComponent
  ],
  imports: [
    routing,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    JsonpModule,
    BrowserModule,
    BrowserAnimationsModule,

    // Material
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,

    // Flex-layout
    FlexLayoutModule
  ],
  providers: [
    AppConfig,
    AuthenticatedGuard,
    NoAuthenticatedGuard,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseService,
      multi: true
    },

    AuthenticationService,
    UserService
  ],
  entryComponents: [DialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
