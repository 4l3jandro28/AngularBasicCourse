import { Component, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MediaMatcher } from '@angular/cdk/layout';

import 'rxjs/add/operator/filter';

import { User } from './_models/user';
import { AuthenticationService } from './_services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isDarkTheme = false;
  isSidenavOpened = false;
  currentUser: User = new User();
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
    public authenticationService: AuthenticationService,
    private router: Router,

    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher) {

    const avatarsSafeUrl = sanitizer.bypassSecurityTrustResourceUrl('./assets/avatars.svg');
    iconRegistry.addSvgIconSetInNamespace('avatars', avatarsSafeUrl);
    this.redirect();  

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  private redirect() {
    if(!this.authenticationService.isAuthenticated()) {
      this.router.navigate(['/login']); 
    }   
  }
}
