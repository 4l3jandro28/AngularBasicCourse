import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppConfig } from '../app.config';
import { User } from '../_models/user';

@Injectable()
export class UserService {

  constructor(private http: HttpClient, private config: AppConfig) { }
 
    getAll() {
        return this.http.get(this.config.apiUrl + '/users').map(response => response);
    }
 
    getById(id: number) {
        return this.http.get(this.config.apiUrl + '/users/' + id).map(response => response);
    }

    isValidEmail(email: string) {
        return this.http.get(this.config.apiUrl + '/users/isValidEmail/' + email).map(response => response);
    }
 
    create(user: User) {
        return this.http.post(this.config.apiUrl + '/users/register', user);
    }
 
    update(user: User) {
        return this.http.put(this.config.apiUrl + '/users/' + user.id, user);
    }
 
    delete(id: number) {
        return this.http.delete(this.config.apiUrl + '/users/' + id);
    }
}
