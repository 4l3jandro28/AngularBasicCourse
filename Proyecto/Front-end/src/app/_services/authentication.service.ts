import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

import { AppConfig } from '../app.config';
import { User } from '../_models/user';

@Injectable()
export class AuthenticationService {

    constructor(
        private http: HttpClient,
        private config: AppConfig) { }

    isAuthenticated(): boolean {
        return localStorage.getItem(this.config.keyCurrentUser) != null;
    }

    getToken(): string {
        return JSON.parse(localStorage.getItem(this.config.keyCurrentUser)).token;
    }

    getCurrentUser(): User {
        return JSON.parse(localStorage.getItem(this.config.keyCurrentUser)) as User;
    }

    login(username: string, password: string) {
        return this.http.post(this.config.apiUrl + '/users/authenticate', { username: username, password: password })
            .map((response: Response) => {
                let user = response;
                if (user) {
                    localStorage.setItem(this.config.keyCurrentUser, JSON.stringify(user));
                }
            });
    }

    logout() {
        localStorage.removeItem(this.config.keyCurrentUser);
    }

}
