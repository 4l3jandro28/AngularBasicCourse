import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  @Output() onToggledTheme = new EventEmitter<void>();
  @Output() onToggledSideav = new EventEmitter<void>();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  toggleTheme() {
    this.onToggledTheme.emit();
  }

  toggleSidenav() {
    this.onToggledSideav.emit();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']); 
  }
}
