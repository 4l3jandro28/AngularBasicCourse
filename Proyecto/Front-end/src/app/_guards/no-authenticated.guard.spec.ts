import { TestBed, async, inject } from '@angular/core/testing';

import { NoAuthenticatedGuard } from './no-authenticated.guard';

describe('NoAuthenticatedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoAuthenticatedGuard]
    });
  });

  it('should ...', inject([NoAuthenticatedGuard], (guard: NoAuthenticatedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
