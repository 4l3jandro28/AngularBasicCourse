export class User {
	id: number;
	avatar: string;
    username: string;
	password: string;
	description: string;
	isAdministrator: boolean;
	isCool: boolean;
    firstName: string;
	lastName: string;
}
