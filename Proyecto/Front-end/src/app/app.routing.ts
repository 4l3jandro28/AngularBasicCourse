import { Routes, RouterModule } from '@angular/router';
 
import { HomeComponent } from './_pages/home/home.component';
import { LoginComponent } from './_pages/login/login.component';
import { RegisterComponent } from './_pages/register/register.component';
import { GridComponent } from './_pages/grid/grid.component';

import { DetailComponent } from './_pages/users/detail/detail.component';
import { IndexComponent } from './_pages/users/index/index.component';

import { AuthenticatedGuard } from './_guards/authenticated.guard';
import { NoAuthenticatedGuard } from './_guards/no-authenticated.guard';
 
const appRoutes: Routes = [
	{ path: '', component: HomeComponent, canActivate: [AuthenticatedGuard] },
	{ path: 'users', component: IndexComponent, canActivate: [AuthenticatedGuard] },
	{ path: 'userDetail/:id', component: DetailComponent, canActivate: [AuthenticatedGuard] },
	{ path: 'grid', component: GridComponent, canActivate: [AuthenticatedGuard] },
	
    { path: 'login', component: LoginComponent, canActivate: [NoAuthenticatedGuard] },
    { path: 'register', component: RegisterComponent, canActivate: [NoAuthenticatedGuard] },
 
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
 
export const routing = RouterModule.forRoot(appRoutes);