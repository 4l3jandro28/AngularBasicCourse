import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, ValidationErrors } from '@angular/forms';
 
import { UserService } from '../../_services/user.service';
import { User } from '../../_models/user';
import { AbstractControl } from '@angular/forms/src/model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  model: User;
  avatars = new Array(16).fill(0).map((_, i) => `svg-${i+1}`);
  hide: boolean;

  constructor(
    private router: Router,
    private userService: UserService) {
      this.model = new User();
  }

  ngOnInit() {
    this.hide = true;
    this.model.avatar = this.avatars[0];

    this.registerForm = new FormGroup({
      avatar: new FormControl('', Validators.required),
      username: new FormControl(
        '', 
        [Validators.required, Validators.email],
        this.shoulBeUnique.bind(this)
      ),
      password: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      description: new FormControl(''),
      isAdministrator: new FormControl(''),
      isCool: new FormControl(''),
    });
  }

  register() {
    this.userService.create(this.model)
      .subscribe(
      data => {
        this.router.navigate(['/login']);
      });
  }

  shoulBeUnique(control: AbstractControl) {
    return this.userService.isValidEmail(control.value).map(response => {
      return response ? null : { shoulBeUnique: true };
    });
  }
}
