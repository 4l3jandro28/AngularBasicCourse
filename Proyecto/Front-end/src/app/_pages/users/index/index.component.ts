import { Component, OnInit } from '@angular/core';
import {MatTableDataSource, MatDialog} from '@angular/material';

import { UserService } from '../../../_services/user.service';
import { User } from '../../../_models/user';
import { AuthenticationService } from '../../../_services/authentication.service';
import { DialogComponent } from '../../../_components/dialog/dialog.component';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  displayedColumns = ['firstName', 'lastName', 'username', 'actions'];
  users = new MatTableDataSource();

  constructor(
    private userService: UserService,
    private dialog: MatDialog,
    public authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    this.loadAllUsers();
  }

  deleteUser(id: number) {
    this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
  }

  updateUser(user: User) {
    this.dialog.open(DialogComponent, {data: user})
        .afterClosed()
        .filter(result => !!result)
        .subscribe(userModified => {
          this.userService.update(userModified).subscribe(data => {
            this.loadAllUsers();
          });
        });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.users.filter = filterValue;
  }

  private loadAllUsers() {
     this.userService.getAll().subscribe(users => { 
       this.users = new MatTableDataSource<User>(users as User[]);
      });
  }

}
