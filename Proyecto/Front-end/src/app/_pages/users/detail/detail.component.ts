import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../../_models/user';
import { UserService } from '../../../_services/user.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  selectedUser;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {
    this.selectedUser = new User();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userService.getById(+params['id']).subscribe(user => this.selectedUser = user);
   });
  }

}
